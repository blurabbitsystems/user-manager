FROM ubuntu:18.04

RUN apt-get update

RUN apt-get install -y python python3.6
RUN update-alternatives --install /usr/bin/python python /usr/bin/python3.6 1

RUN apt-get install -y python-pip python3-pip
RUN update-alternatives --install /usr/bin/pip pip /usr/bin/pip3 1

RUN apt-get install -y jq libmysqlclient-dev netcat

WORKDIR /code
COPY ./requirements.txt /code/requirements.txt

RUN pip install -r requirements.txt
COPY . /code

CMD ["sh", "runner.sh"]
