#!bin/sh

export DB_HOST=$(jq -r '.DB_HOST // "127.0.0.1"' .config/env.json)

while ! nc -zw1 $DB_HOST 3306
do
  echo "Waiting for connection to rds-host: ${DB_HOST}"
  sleep 1
done

python -m scripts.create-database
# python manage.py makemigration
python manage.py migrate
python manage.py runserver 0.0.0.0:8000
