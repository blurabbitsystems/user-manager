from django.urls import path

from .views import SingleUserView, UserView, UserValidationView

app_name = 'api'


urlpatterns = [
    path('users/', UserView.as_view()),
    path('users/<str:guid>', SingleUserView.as_view()),
    path('users/checkpassword/', UserValidationView.as_view()),
]
