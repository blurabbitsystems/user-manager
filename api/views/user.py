from django.shortcuts import get_object_or_404
from django.http import Http404
from rest_framework.generics import  RetrieveUpdateAPIView, ListCreateAPIView
from rest_framework.views import APIView
from rest_framework.response import Response

from api.models import User
from api.serializers.user import UserSerializer


class UserView(ListCreateAPIView):
    serializer_class = UserSerializer
    queryset = User.objects.all()


class SingleUserView(RetrieveUpdateAPIView):
    lookup_field = 'guid'
    serializer_class = UserSerializer
    queryset = User.objects.all()


class UserValidationView(APIView):

    def post(self, request):
        username = request.data.get('username')
        password = request.data.get('password')

        if not username or not password:
            raise Http404
            
        user = get_object_or_404(User, username=username)
        if not user.check_password(password):
            raise Http404

        serializer = UserSerializer(user)
        return Response(serializer.data)
