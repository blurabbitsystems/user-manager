from .. import User
from ..utils import generate_guid


def make_user(**kwargs):
    defaults = {
        'fullname': generate_guid(to_hex=True),
        'username': generate_guid(to_hex=True)[:31],
        'password': generate_guid(to_hex=True),
    }
    defaults.update(kwargs)
    return User.objects.create(**defaults)
