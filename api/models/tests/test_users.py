from django.test import TestCase

from .utils import make_user
from .. import User


class TestUser(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.user = make_user(username='test.user', password='pAs5w0rd')
    
    def test_check_password(self):
        assert self.user.check_password('pAs5w0rd') is True
        assert self.user.check_password('pAs5w0rD') is False
