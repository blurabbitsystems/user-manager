from django.contrib.auth.hashers import make_password, check_password
from django.db import models
from .utils import generate_guid


class User(models.Model):
    guid = models.UUIDField(unique=True, default=generate_guid, editable=False)
    username = models.CharField(unique=True, max_length=31)
    password = models.CharField(max_length=127)
    fullname = models.CharField(max_length=255, blank=True, null=True)

    def __str__(self):
        return '[{0.fullname}] {0.username}'.format(self)

    def save(self, *args, **kwargs):
        self.password = make_password(self.password)
        super().save(*args, **kwargs)

    def check_password(self, password):
        return check_password(password, self.password)
