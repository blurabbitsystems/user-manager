from django.test import TestCase, Client

from api.models.tests.utils import make_user
from api.models.utils import generate_guid


class TestUsersEndpoints(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.client = Client()
        cls.user_guid = generate_guid()
        cls.username = 'user.name'
        cls.password = 'p4s5woRd'

        make_user(username=cls.username, password=cls.password, guid=cls.user_guid)
        make_user()

    def test_get_all_users(self):
        resp = self.client.get('/api/users/')
        assert resp.status_code == 200

        users = resp.json()
        assert len(users) == 2
        assert list(users[0].keys()) == ['guid', 'fullname', 'username']

    def test_get_single_user(self):
        resp = self.client.get('/api/users/{}'.format(self.user_guid))
        assert resp.status_code == 200

        user = resp.json()
        assert user['guid'] == self.user_guid.hex
        assert user['username'] == self.username

    def test_check_password(self):
        resp = self.client.post('/api/users/checkpassword/', {'username': self.username, 'password': self.password}, content_type='application/json')
        assert resp.status_code == 200

        user = resp.json()
        assert user['guid'] == self.user_guid.hex

    def test_check_password__with_invalid_password(self):
        resp = self.client.post('/api/users/checkpassword/', {'username': self.username, 'password': 'iNv4l1D'}, content_type='application/json')
        assert resp.status_code == 404
