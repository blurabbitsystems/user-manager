from rest_framework import serializers
from api.models import User


class UserSerializer(serializers.ModelSerializer):
    guid = serializers.UUIDField(format='hex', read_only=True)
    password = serializers.CharField(write_only=True)

    class Meta:
        model = User
        fields = ['guid', 'fullname', 'username', 'password']
