from django.contrib import admin

from .models import User


@admin.register(User)
class UserAdmin(admin.ModelAdmin):
    list_display = ['guid', 'fullname', 'username']
    list_display_links = ['guid']
    search_fields = ['guid', 'fullname', 'username']
